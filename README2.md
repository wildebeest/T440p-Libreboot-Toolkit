## Step 1: Download Libreboot ROMs
- **ROM**: [libreboot-20240126_t440pmrc_12mb.tar.xz](https://mirrors.mit.edu/libreboot/testing/20240126/roms/libreboot-20240126_t440pmrc_12mb.tar.xz)
- **SHA512**: [libreboot-20240126_t440pmrc_12mb.tar.xz.sha512](https://mirrors.mit.edu/libreboot/testing/20240126/roms/libreboot-20240126_t440pmrc_12mb.tar.xz.sha512)
- **Signature**: [libreboot-20240126_t440pmrc_12mb.tar.xz.sig](https://mirrors.mit.edu/libreboot/testing/20240126/roms/libreboot-20240126_t440pmrc_12mb.tar.xz.sig)

## Step 2: Download Libreboot's Signing Key
- **Key**: [lbkey.asc](https://libreboot.org/lbkey.asc)
- **Command**: `gpg --import lbkey.asc`

## Step 3: Verify Libreboot ROMs
- **Verify Signature**: `gpg --verify libreboot-20240126_t440pmrc_12mb.tar.xz.sig libreboot-20240126_t440pmrc_12mb.tar.xz`
- **Checksum**: `sha512sum -c libreboot-20240126_t440pmrc_12mb.tar.xz.sha512`

## Step 4: Download lbmk
- **Clone link**: `git clone https://codeberg.org/libreboot/lbmk.git`
- **Enter directory**: `cd lbmk`

## Step 5: Build Dependencies
- **Command**: `sudo ./build dependencies debian`

## Step 6: Inject Vendor Files
- **Inject blobs into the .tar.xz file**: `./vendor inject ../libreboot-20240126_t440pmrc_12mb.tar.xz`

This will automatically inject and extract the .rom files into lbmk/bin/release/t440pmrc_12mb.

## Step 7: Verify Blobs
- **Update Trees**: `./update trees -b coreboot utils`
- **Navigate to the Directory**: Change into the directory where the .rom files with the now injected blobs are located: 
`cd lbmk/bin/release/t440pmrc_12mb`
- **List ROMs**: `ls`
- **Print ROM**: (Look for mrc.bin in the output) `../../.././cbutils/default/cbfstool libreboot.rom print`
- **Dump the .bin file to verify if MRC was inserted correctly**: `../../.././cbutils/default/ifdtool -x libreboot.rom`
- **Run hexdump on the .bin file**: `hexdump flashregion_2_intel_me.bin`

e.g. libreboot.rom = seabios_withgrub_t440pmrc_12mb_libgfxinit_corebootfb_usqwerty.rom

If you don't see '0xFF' anywhere in the hexdump output, that means the blobs have inserted correctly! You're now ready to flash!

## Step 8: Choose ROM to Flash
- **Recommended ROMs to choose**: `seabios_withgrub_t440pmrc_12mb_libgfxinit_corebootfb_usqwerty.rom` or `grub_t440pmrc_12mb_libgfxinit_corebootfb_usqwerty.rom`

## Next Steps
- Follow the flashing instructions from the guide I provided in the main guide.
